![Nova College Logo](resources/logo.png)

# Linux, Apache, MySQL, PHP and XDebug (LAMP+XDebug) Vagrant Machine

Features:

* php7.1
* Apache2
* MySQL 5.5
* XDebug
* phpMyAdmin

**NOTE: There is a HOWTO_DEBUG_WAMP_XAMPP.md for the developers using WAMPServer or XAMPP!**

## Ensure the following tools are installed:

_(Select the correct version matching your OS and CPU Architecture)_

1. [VirtualBox](https://www.virtualbox.org/)
1. [Vagrant](https://www.vagrantup.com/downloads.html)

Then it's a matter of choice which development tool you like with PHP debug abilities. This readme describes a few:

Jetbrains:

* [IntelliJ Ultimate](https://www.jetbrains.com/idea/) (Don't forget to install the PHP Jetbrains plugin)
* [PHPStorm](https://www.jetbrains.com/phpstorm/)

## Spin up your vagrant box

1. Open a terminal or powershell and change your directory to the root of this project (Where the Vagrant file is situated)
1. Execute the command: `vagrant up`

_(Please be patient. The first time this box is started it will take a bit of time to download and provision the box)_

Tips (powershell / terminal):

_Change to the root directory of your project first_

* When started you can stop the box by executing: `vagrant halt`
* When started and you want to restart: `vagrant reload`
* Login through ssh: `vagrant ssh` (This ensures you're logged at the terminal of the virtual machine)
* Remove the box: `vagrant destroy` (Be Carefull! Removes your virtual machine.)

For more information: [Getting started](https://www.vagrantup.com/intro/getting-started/)

## Configure Jetbrains PHPStorm or IntelliJ (with PHP plugin)

### Install PHP plugin

`Preferences` => `Plugins` => `Install Jetbrains Plugin` => Search for `PHP` and install

### Run configuration (Debug configuration) 

#### Ensure Xdebug port is correctly configured:

`Preferences` => `Languages & Frameworks` => `PHP` => `Debug` => `Xdebug` => Debug port: `9000`

#### Configure PHP Server:

1. Navigate to: `Preferences` => `Languages & Frameworks` => `PHP` => `Servers`
1. Add a new entry by clicking on `+`
1. Select the checkbox: `Use path mappings...`
1. And these settings
    1. Fill in a useful name, e.g: `Vagrant debug`
    1. Host: `localhost`
    1. Port: `8080`
    1. Debugger: `Xdebug`
    1. Select the (local) File/Directory `[root of your project]/public`
    1. Select the corresponding `Absolute path on the server` and fill in: `/var/www/app/public`

#### Create a Run configuration

1. `Run` => `Edit Configuration` => Click on the `+` sign => Choose: `PHP Web Application`
1. Give a corresponding name e.g: `Run index`
1. Select option: `Single instance only`
1. `Configuration` => `Server` pull down => And choose the earlier configured Server, e.g: `Vagrant debug`
1. Start url: `/index.php` (Which results in `http://localhost:8080/index.php`)
1. Choose a browser of your liking (Firefox or Chrome for instance)
1. Click the OK button

_(Now you're ready to debug your php code!)_



# For hackers:

* Your public folder is exposed and served at: [http://localhost:8080](http://localhost:8080)
* phpMyAdmin is available in the public folder and served at: [http://localhost:8080/phpmyadmin](http://localhost:8080/phpmyadmin)
* There is an empty development database created called `app`
    * Login with username:`root`, password: `root`
* See the `scripts/provision-vagrant.sh` (here you can find `allthethings` being done when provisioning the box the first time)
* Ports exposed and available at localhost (Host Machine):
    * 8080 (HTTP)
    * 3306 (MySQL)
* Login through ssh and access the Guest Machine: `vagrant ssh`
* Mapped folder: `./` (Host Machine) => `/var/www/app` (Guest Machine)
* There is a Composer file created in the root of the project folder, when you need to operate `composer`, please do the following:
    1. Go to powershell or terminal and change directory to the root of this project (Host machine)
    1. Execute `vagrant ssh` (This will log you in the Virtual environment, the Guest machine's terminal)
    1. Execute `cd /var/www/app/` (Mapped to the root of this project)
    1. Execute `composer install` (Install all dependencies mentioned in `composer.json`)
    
