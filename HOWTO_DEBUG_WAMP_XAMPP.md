

# Setup PHP debugging in IntelliJ or PHPStorm (Jetbrains)


This needs to be installed first

* IntelliJ / PHPStorm
* WAMPServer / XAMPP


## WAMPServer

When WAMPServer is installed we need to double check which version of PHP is running.

By left click on the WAMPServer icon on the system tray in Windows: `PHP` => `Version`

_Example:_

![PHP Version](resources/wamp-php-version.png)

Now you know which version of PHP you have.
Download the correct version of XDebug from: [XDebug download](https://xdebug.org/download.php)

Keep in mind the architecture of you computer (64bit/32bit) and choose for the `Thread Safe (TS) version`

When downloaded copy/paste the DLL file to this location: `C:\wamp64\bin\php\php7.1.9\zend_ext\`

### Changes to make in the `php.ini` file:

Open the `php.ini` file by left clicking on the WAMPServer icon: `PHP` => `php.ini`

_Example:_

![PHP ini](resources/wamp-php-ini.png)

Scroll all the way to the bottom and add this:

```
; XDEBUG Extension
[xdebug]
zend_extension ="c:/wamp64/bin/php/php7.1.9/zend_ext/php_xdebug-[your downloaded version].dll"

xdebug.remote_enable = On
xdebug.profiler_enable = off
xdebug.profiler_enable_trigger = Off
xdebug.profiler_output_name = cachegrind.out.%t.%p
xdebug.profiler_output_dir ="c:/wamp64/tmp"
xdebug.show_local_vars=0
xdebug.remote_handler=dbgp
xdebug.remote_host=localhost
xdebug.remote_port=9000
```

_Save the file and restart WAMPServer!_


## XAMPP

When XAMPP is installed we need to figure out which PHP version we have.

A fast way to check this is to create a test php file `C:\xampp\htdocs\debugtest\index.php` and ensure this piece of code is stored:

```php
<?php

phpinfo();
```

Navigate in the browser to: [http://localhost/debugtest/index.php](http://localhost/debugtest/index.php), and take note of the php version:

Now download the correct version of XDebug from: [xdebug.org downloads](https://xdebug.org/download.php).

Copy/paste the DLL file to `C:\xampp\php\ext`.

Open `C:\xampp\php\php.ini` for editing with notepad or something similar.
 
Change:

`output_buffering=4096` to: `output_buffering=Off`

Then ensure you have this at the bottom of the file:

```
[XDebug]
zend_extension = "c:/xampp/php/ext/php_xdebug-[your downloaded version].dll"
xdebug.remote_enable = On
xdebug.profiler_enable = off
xdebug.profiler_enable_trigger = Off
xdebug.profiler_output_name = cachegrind.out.%t.%p
xdebug.profiler_output_dir ="c:/wamp64/tmp"
xdebug.show_local_vars=0
xdebug.remote_handler=dbgp
xdebug.remote_host=localhost
xdebug.remote_port=9000
```

Restart Apache and reload [http://localhost/debugtest/index.php](http://localhost/debugtest/index.php). Search for XDebug, like this:

![PHP version](resources/xamp-phpinfo-xdebug.png)

## Settings to debug PHP in IntelliJ or PHPStorm

**Only for IntelliJ**

NOTE: First install the Jetbrains plugin:
`File` => `Settings` => `Plugins` => `Install Jetbrains Plugin`

When all goes fine, restart the IDE.

**IntelliJ / PHPStorm**

### Choose the corresponding PHP version 

1. Open: `File` => `Settings` => `Languages & Frameworks` => `PHP`
1. Choose your corresponding PHP version

### Configure a Server

1. Open: `File` => `Settings` => `Languages & Frameworks` => `PHP` => `Servers`
1. Add 1 by clicking on the green plus button
1. Give yours a meaningfull name e.g: `Server`
1. Ensure: `host`: `localhost`, `port`: `80`, `debugger`: `xdebug`

### Prevent halting on the first line of code (Switch off: `Break at first line`)

1. Open: `File` => `Settings` => `Languages & Frameworks` => `PHP` => `Debug`
1. Search for this option `Break at first line..` , switch it off.

### Add a (run) configuration (you can make several ones, once you've get the hang of it!)


1. Go to: `Run` => `Edit Configurations`
1. Add one by clicking on the green plus button
1. Search for `PHP Web Page` and acknoledge
1. Choose the earlier made `Server`
1. Configure Start URL: `/[relative path to your code]/[your php file]`
1. Place a breakpoint in the side line of your code somewhere
1. Click: `Run` => `Debug`

_Example:_

![On breakpoint](resources/jetbrains-debug-breakpoint.png)

_IntelliJ / PHPStorm will open Chrome with your page and pause on the breakpoint!_


# Sources

* [WAMPServer](http://www.wampserver.com/en/)
* [XAMPP](https://www.apachefriends.org/index.html)
* [Installing Xdebug for XAMPP with PHP 7.x](https://gist.github.com/odan/1abe76d373a9cbb15bed)


